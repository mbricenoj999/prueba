import { Injectable } from '@angular/core';
import { Persona } from '../persona';

@Injectable({
  providedIn: 'root',
})
export class MedicoService {
  constructor() {}


  getAllMedicos(){
    let json = localStorage.getItem("medicos")||'[]';
    return (JSON.parse(json))
  }

  crearMedico(persona: Persona) {
    let medico = {
      ...persona,
      cargo: 'medico',
    };

    console.log(medico);
    let existentes : any[]=[];
    existentes=(this.getAllMedicos());
    let repetido = false;

    existentes?.forEach((medico:any) => {
      if(medico.numeroDocumento===persona.numeroDocumento){
        repetido= true;
      }
    });
    if (repetido){
      return repetido;
    }
    existentes.push(medico);
    this.saveStorage(JSON.stringify(existentes))
    return repetido;
  }

  editar(documento: string, persona: Persona) {
    let medicos:any[] = this.getAllMedicos();

    let i = medicos.findIndex(medico=>medico.numeroDocumento==documento);
    medicos[i]={
      ...medicos[i],
      ...persona,
      numeroDocumento:documento
    }

    this.saveStorage(JSON.stringify(medicos));
  }

  saveStorage(valor: string) {
    localStorage.setItem('medicos', valor);
  }

  getInfo(documento:string){
    let medicos = this.getAllMedicos();
    let valueToReturn=null;
    medicos.forEach((medico:any) => {
      if(medico["numeroDocumento"]==documento){
        valueToReturn=medico;
      }
    });
    return valueToReturn;
  }

  eliminar(documento:string){
    let medicos:any[]=this.getAllMedicos();
    medicos=medicos.filter(medico=>
      medico.numeroDocumento !== documento
    );
    this.saveStorage(JSON.stringify(medicos));
  }
}
