import { Injectable } from '@angular/core';
import { Persona } from '../persona';

@Injectable({
  providedIn: 'root',
})
export class PacienteService {
  constructor() {}

  saveStorage(valor: string) {
    localStorage.setItem('pacientes', valor);
  }

  getAllPacientes() {
    let json = localStorage.getItem('pacientes') || '[]';
    return JSON.parse(json);
  }

  crearPaciente(persona: Persona) {
    let paciente = {
      ...persona,
      cargo: 'paciente',
    };

    let existentes: any[] = [];
    existentes = this.getAllPacientes();
    let repetido = false;

    existentes?.forEach((paciente: any) => {
      if (paciente.numeroDocumento === persona.numeroDocumento) {
        repetido = true;
      }
    });
    if (repetido) {
      return repetido;
    }
    existentes.push(paciente);
    this.saveStorage(JSON.stringify(existentes));
    return repetido;
  }

  editar(documento: string, persona: Persona) {
    let pacientes:any[] = this.getAllPacientes();

    let i = pacientes.findIndex(paciente=>paciente.numeroDocumento==documento);
    pacientes[i]={
      ...pacientes[i],
      ...persona,
      numeroDocumento:documento
    }

    this.saveStorage(JSON.stringify(pacientes));
  }

  getInfo(documento: string) {
    let pacientes = this.getAllPacientes();
    let valueToReturn = null;
    pacientes.forEach((paciente: any) => {
      if (paciente.numeroDocumento === documento) {
        valueToReturn = paciente;
      }
    });
    return valueToReturn;
  }

  eliminar(documento: string) {
    let pacientes = this.getAllPacientes();
    pacientes.forEach((paciente: any) => {
      if (paciente.numeroDocumento === documento) {
        paciente = null;
      }
    });
    this.saveStorage(JSON.stringify(pacientes));
  }
}
