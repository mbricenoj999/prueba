import { Component } from '@angular/core';
import { Persona } from './persona';
import { MedicoService } from './services/medico.service';
import { PacienteService } from './services/paciente.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'pueba';
  medicos: any[] = [];
  pacientes: any[] = [];

  constructor(
    public medicosService: MedicoService,
    public pacienteService: PacienteService
  ) {
    this.medicos = this.medicosService.getAllMedicos();
    this.pacientes = pacienteService.getAllPacientes();
  }

  agregar(
    primerN: string,
    segundoN: string,
    primerA: string,
    segundoA: string,
    tipoDoc: string,
    numeroDoc: string,
    fecha: string,
    cargo: string
  ) {
    let persona: Persona = {
      primerNombre: primerN,
      segundoNombre: segundoN,
      primerApellido: primerA,
      segundoApellido: segundoA,
      tipoDocumento: tipoDoc,
      numeroDocumento: numeroDoc,
      fechaexpedicion: fecha,
    };

    if (cargo === 'Medico' && !this.pacienteService.getInfo(numeroDoc)) {
      if (this.medicosService.crearMedico(persona)) {
        console.log('verifique numero documento repetido', this.medicos);
      } else {
        console.log('agregado exitosamente');
      }
    } else console.log('documento existente verifique nuevamente');
    if (cargo === 'Paciente' && !this.medicosService.getInfo(numeroDoc)) {
      if (this.pacienteService.crearPaciente(persona)) {
        console.log('verifique numero documento repetido', this.medicos);
      } else {
        console.log('agregado exitosamente');
      }
    }
  }

  ver() {
    this.medicos = this.medicosService.getAllMedicos();
    console.log('medicos', this.medicos);
    console.log('pacentes', this.pacientes);
  }

  consultar(documento: string) {
    let persona =
      this.pacienteService.getInfo(documento) ||
      this.medicosService.getInfo(documento) ||
      'persona inexistente';
    console.log(persona);
  }

  eliminar(documento: string) {
    if (this.pacienteService.getInfo(documento)) {
      this.pacienteService.eliminar(documento);
      this.pacientes = this.pacienteService.getAllPacientes();
    }
    if (this.medicosService.getInfo(documento)) {
      this.medicosService.eliminar(documento);
      this.medicos = this.medicosService.getAllMedicos();
    }
  }

  editar(
    documento: string,
    primerNombre: string,
    segundoNombre: string,
    primerApellido: string,
    segundoApellido: string,
    tipoDocumento: string,
    fechaexpedicion: string
  ) {
    if (this.pacienteService.getInfo(documento)) {
      this.pacienteService.editar(documento, {
        primerNombre,
        segundoNombre,
        primerApellido,
        segundoApellido,
        tipoDocumento,
        numeroDocumento:documento,
        fechaexpedicion,
      });
      this.pacientes = this.pacienteService.getAllPacientes();
    }
    if (this.medicosService.getInfo(documento)) {
      this.medicosService.editar(documento, {
        primerNombre,
        segundoNombre,
        primerApellido,
        segundoApellido,
        tipoDocumento,
        numeroDocumento:documento,
        fechaexpedicion,
      });
      this.medicos = this.medicosService.getAllMedicos();
    }
  }
}
