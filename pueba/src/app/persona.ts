export interface Persona {
  primerNombre: string;
  segundoNombre: string;
  primerApellido: string;
  segundoApellido:string;
  tipoDocumento: string;
  numeroDocumento: string;
  fechaexpedicion: string;
}
